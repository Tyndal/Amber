// script.js

// Menu

$(document).scroll(function() {
  var scrollTop = $(window).scrollTop();
  console.log(scrollTop);
  if (scrollTop >= 47) {
    $("nav").addClass("scroll-nav");
  } else {
    $("nav").removeClass("scroll-nav");
  }
});

//Back to the top button

//Quote section carousel

$(function() {
  var $carouselList = $("#carousel ul");

  setInterval(changeSlides, 8000);

  function changeSlides() {
    $carouselList.animate({ marginLeft: -810 }, 2000, moveFirstSlide);
  }

  function moveFirstSlide() {
    var $firstItem = $carouselList.find("li:first");
    var $lastItem = $carouselList.find("li:last");
    $lastItem.after($firstItem);
    $carouselList.css({ marginLeft: 10 });
  }
});

//Team section

function toggleDiv() {
  let divElement = document.getElementById("description-1");

  divElement.style.transition = "all 1s";

  if (divElement.style.opacity === "0") {
    divElement.style.opacity = "1";
  } else {
    divElement.style.opacity = "0";
  }
}

let btnOne = document.getElementById("team-profile-btn-1");
btnOne.addEventListener("click", toggleDiv);

// Hero carousel

// $(function() {
//   var $carouselList = $("#hero-carousel ul");

//   setInterval(changeSlides, 5000);

//   function changeSlides() {
//     $carouselList.animate({ marginLeft: -2000 }, 2000, moveFirstSlide);
//   }

//   function moveFirstSlide() {
//     var $firstItem = $carouselList.find("li:first");
//     var $lastItem = $carouselList.find("li:last");
//     $lastItem.after($firstItem);
//     $carouselList.css({ marginLeft: 0 });
//   }
// });
